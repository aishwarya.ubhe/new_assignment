function boilWater(){};
let generalMeal = "pasta";
let cookout={
  grill: function(duration, meat, name){
    console.log("Thanks " + name + "! Your " + meat + "will be ready in " + duration + "minutes.");
  }
}
let cookBurger = cookout.grill.bind(cookout, 15, "burger");

let cookChicken = cookout.grill.bind(cookout, 20, "chicken");

let cookSteak = cookout.grill.bind(cookout, 10, "steak");
cookBurger("Jack")
cookSteak("Jill")
cookChicken("David")
cookout = {
  drink:"soda",
  grill: function(meal) {
    console.log("I am going to fire up the grill to cook " + meal + " with " +this.drink +" to drink!");
  }
}
let fancyDinner = {
  drink: "wine",
  useOven: function() {}
}
cookout.grill("steak");
cookout.grill.call(fancyDinner, "steak");
cookout = {
  mealOrders: ["chicken", "burger", "burger", "steak", "chicken"],
  grill: function() {
    let args = Array.prototype.slice.call (arguments);

    console.log("I am going to cook :" + args.join(","));
  }
}
cookout.grill.apply(cookout, this.mealOrders);
