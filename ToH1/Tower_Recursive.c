#include <stdio.h>
void h_tower(int disks,char s_rod, char d_rod, char f_rod)
{
    if(disks==1)
    {
        printf("\nMove the disk 1 from %c to %c",s_rod,d_rod);
        return;
    }
    h_tower(disks-1,s_rod,f_rod,d_rod);
    printf("\nMove the disk %d from %c to %c",disks,s_rod,d_rod);
    h_tower(disks-1,f_rod,d_rod,s_rod);

}
int main()
{
    int disks;
    printf("Enter no of disks");
    scanf("%d",&disks);
    h_tower(disks,'a','b','c');
    return 0;
}
