var animal=function(name,age,type)
{
  this.name=name;
  this.age=age;
  this.type=type;
}
animal.prototype.getType=function()
{
  console.log("Type of animal is"+this.type);
}

var dog=function(bark,name,age,type)
{
  animal.call(this,name,age,type);
  this.bark=bark;
}

var human=function(earnMoney,name,age,type)
{
  animal.call(this,name,age,type);
  this.earnMoney=earnMoney;
}

dog.prototype=Object.create(animal.prototype);
dog.prototype.constructor=dog;

human.prototype=Object.create(animal.prototype);
human.prototype.constructor=human;

//var animalInstance=new animal('lucci',10,'dog');
var dogInstance=new dog('yes','lucci',10,'dog');
var humanInstance=new human('yes','aish',20,'human');
